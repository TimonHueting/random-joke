import React, { Fragment, useState, useEffect } from "react";

import classes from "./Joke.module.css";
import Card from "../UI/Card";
import Button from "../UI/Button";

const Joke = () => {
  const [data, setData] = useState({
    id: null,
    joke: "",
    status: null,
  });
  const [newJoke, setNewJoke] = useState();

  useEffect(() => {
    const fetchJoke = async () => {
      await fetch("https://icanhazdadjoke.com/", {
        headers: {
          Accept: "application/json",
        },
      })
        .then((res) => res.json())
        .then((res) => {
          setData(res);
        });
    };
    fetchJoke();
  }, [newJoke]);

  return (
    <Fragment>
      {data.id ? (
        <Card>
          <div className={classes.joke}>{data.joke}</div>
        </Card>
      ) : (
        <Card>
          <div className={classes.joke}>
            Sorry.. I can't think of a joke at the moment.
          </div>
        </Card>
      )}
      <Button onClick={setNewJoke}>Laugh again</Button>
    </Fragment>
  );
};

export default Joke;
