import classes from "./Footer.module.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGitlab } from "@fortawesome/free-brands-svg-icons";

const Footer = () => {
  const gitlabIcon = <FontAwesomeIcon icon={faGitlab} />;
  return (
    <div className={classes.footer}>
      <p>
        Created by Timon Hueting - &nbsp;
        <a
          target="_blank"
          rel="noreferrer"
          href="https://gitlab.com/TimonHueting"
        >
          {gitlabIcon}
        </a>
      </p>
    </div>
  );
};

export default Footer;
