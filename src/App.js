import React, { Fragment } from "react";

import Joke from "./components/Jokes/Joke";
import Footer from "./components/Layout/Footer";

function App() {
  return (
    <Fragment>
      <div className="container">
        <h1>Have a laugh!</h1>
        <Joke />
      </div>
      <Footer />
    </Fragment>
  );
}

export default App;
